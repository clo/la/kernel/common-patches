From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Ramji Jiyani <ramjiyani@google.com>
Date: Thu, 25 Nov 2021 00:51:55 +0000
Subject: ANDROID: GKI: Add script to generate symbol protection headers

Called By: KERNEL_SRC/kernel/Makefile if CONFIG_MODULE_SIG_PROTECT=y

Generates headers required by gki_modules.c from symbol lists:

gki_module_protected.h: from android/abi_gki_modules_protected
gki_module_exported.h: from android/abi_gki_modules_exports

Bug: 200082547
Test: Treehugger
Signed-off-by: Ramji Jiyani <ramjiyani@google.com>
Change-Id: Ibcc6e6fe0ad6c7850d48f7c0a283c7f9b06e4456
(cherry picked from commit 23cd26aab14d813fd73eced18988bae06d5b9334)
(cherry picked from commit 31d5735baf0b20dd6a90bd8fcd1fa73a6531a62b)
---
 scripts/gen_gki_modules_headers.sh | 96 ++++++++++++++++++++++++++++++
 1 file changed, 96 insertions(+)
 create mode 100755 scripts/gen_gki_modules_headers.sh

diff --git a/scripts/gen_gki_modules_headers.sh b/scripts/gen_gki_modules_headers.sh
new file mode 100755
--- /dev/null
+++ b/scripts/gen_gki_modules_headers.sh
@@ -0,0 +1,96 @@
+#!/bin/bash
+# SPDX-License-Identifier: GPL-2.0-only
+#
+# Copyright 2021 Google LLC
+# Author: ramjiyani@google.com (Ramji Jiyani)
+#
+
+#
+# Generates hearder files for GKI modules symbol and export protections
+#
+# Called By: KERNEL_SRC/kernel/Makefile if CONFIG_MODULE_SIG_PROTECT=y
+#
+# gki_module_exported.h: Symbols protected from _export_ by unsigned modules
+# gki_module_protected.h: Symbols protected from _access_ by unsigned modules
+#
+# If valid symbol file doesn't exists then still generates valid C header files for
+# compilation to proceed with no symbols to protect
+#
+
+# Collect arguments from Makefile
+TARGET=$1
+SRCTREE=$2
+
+set -e
+
+#
+# Common Definitions
+#
+
+#
+# generate_header():
+# Args: $1 = Name of the header file
+#       $2 = Input symbol list
+#       $3 = Symbol type (protected/exported)
+#
+generate_header() {
+	local header_file=$1
+	local symbol_file=$2
+	local symbol_type=$3
+
+	echo "  GEN     ${header_file}"
+	if [ -f "${header_file}" ]; then
+		rm -f -- "${header_file}"
+	fi
+
+	# Find Maximum symbol name length if valid symbol_file exist
+	if [  -s "${symbol_file}" ]; then
+		# Skip 1st line (symbol header), Trim white spaces & +1 for null termination
+		local max_name_len=$(awk '
+				{
+					$1=$1;
+					if ( length > L && NR > 1) {
+						L=length
+					}
+				} END { print ++L }' "${symbol_file}")
+	else
+		# Set to 1 to generate valid C header file
+		local max_name_len=1
+	fi
+
+	# Header generation
+	cat > "${header_file}" <<- EOT
+	/*
+	 * DO NOT EDIT
+	 *
+	 * Build generated header file with GKI module symbols/exports
+	 */
+
+	#define NO_OF_$(printf ${symbol_type} | tr [:lower:] [:upper:])_SYMBOLS \\
+	$(printf '\t')(sizeof(gki_${symbol_type}_symbols) / sizeof(gki_${symbol_type}_symbols[0]))
+	#define MAX_$(printf ${symbol_type} | tr [:lower:] [:upper:])_NAME_LEN (${max_name_len})
+
+	static const char gki_${symbol_type}_symbols[][MAX_$(printf ${symbol_type} |
+							tr [:lower:] [:upper:])_NAME_LEN] = {
+	EOT
+
+	# If a valid symbol_file present add symbols in an array except the 1st line
+	if [  -s "${symbol_file}" ]; then
+		sed -e 1d -e 's/^[ \t]*/\t"/;s/[ \t]*$/",/' "${symbol_file}" >> "${header_file}"
+	fi
+
+	# Terminate the file
+	echo "};" >> "${header_file}"
+}
+
+if [ "$(basename "${TARGET}")" = "gki_module_protected.h" ]; then
+	# Sorted list of protected symbols
+	GKI_PROTECTED_SYMBOLS="${SRCTREE}/android/abi_gki_modules_protected"
+
+	generate_header "${TARGET}" "${GKI_PROTECTED_SYMBOLS}" "protected"
+else
+	# Sorted list of exported symbols
+	GKI_EXPORTED_SYMBOLS="${SRCTREE}/android/abi_gki_modules_exports"
+
+	generate_header "${TARGET}" "${GKI_EXPORTED_SYMBOLS}" "exported"
+fi
